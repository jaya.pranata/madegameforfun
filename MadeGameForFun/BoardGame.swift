//
//  BoardGame.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import SwiftUI
import Network
struct BoardGame:View{
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var moves:[String] = Array(repeating: "", count: 10)
    @State var isAnimateWrong = false
    @ObservedObject var cards = CardManager(numberOfPair: 6)
    @State var previousIndex = -1
    @State var chekFinish = 0
    @State var showingAlert = false
    @State var isPlayerHost = true
    @State var scorePlayerOne = 0
    @State var scorePlayerTwo = 0
    @State var winnerMessage = ""
    @State var playerOneName = "Player One"
    @State var playerTwoName = "Player Two"
    @State var isStart = false
    @State var isYourTurn = true
    @State var isNetwork = false
    @State var isWaitingForPlayer = true
    @State var result:NWBrowser.Result?
    var parent:FindRoom?
    var body: some View {
        VStack{
            if isStart{
                if !isNetwork{
                    Text(isYourTurn == true ? "\(playerOneName)'s Turn" : "\(playerTwoName)'s Turn")
                        .font(.title)
                }else{
                    Text(isYourTurn == true ? "Your's Turn" : "Enemy's Turn")
                        .font(.title)
                }
            }else{
                Text(isWaitingForPlayer ? "Waiting for other player" : "Waiting for game to start")
                    .font(.title)
                
                if !isWaitingForPlayer && isPlayerHost{
                    Button("Start The Game") {
                        isStart = true
                        cards.generateCard(numberOfPair: 6)
                        setName()
                        encodeMessageSent(type: "init", listCard: cards.listCard, index: -1)
                    }.frame(width: UIScreen.main.bounds.width - 60 ,  alignment: .center)
                    .padding(.vertical, 20)
                    .background(Color.blue)
                    .foregroundColor(Color.white)
                    .cornerRadius(50)
                }
            }
            
            LazyVGrid(columns: Array(repeating: GridItem(.flexible(),spacing: 15), count: 3)){
                ForEach(0..<cards.listCard.count,id: \.self){index in
                    ZStack{
                        Color.blue
                        
                        Color.white
                            .opacity(cards.listCard[index].isFlipped == false ? 1 : 0)
                        Text(cards.listCard[index].isFlipped == true ? cards.listCard[index].imageName : "")
                            .font(.system(size: 55))
                            .fontWeight(.black)
                            .foregroundColor(.white)
                            .opacity(cards.listCard[index].isFlipped == true ? 1 : 0)
                    }
                    .frame(width: getWidth(), height: getWidth())
                    .cornerRadius(15)
                    .rotation3DEffect(
                        .init(degrees: cards.listCard[index].isFlipped == true ? 180 : 0 ) ,
                        axis: (x: 0.0, y: 1.0, z: 0.0),
                        anchor: .center,
                        anchorZ: 0.0,
                        perspective: 1.0
                    )
                    .onTapGesture(perform: {
                        if !isNetwork{
                            checkCardClicked(index: index)
                        }else if isYourTurn{
                            //sharedConnection?.sendMessage("sadasijsadid")
                            encodeMessageSent(type: "move", listCard: cards.listCard, index: index)
                            checkCardClicked(index: index)
                        }
                    })
                    .alert(isPresented:$showingAlert) {
                        alertGameFinish()
                    }
                }
            }
            .preferredColorScheme(.dark)
            .padding(15)
            
            
            if isStart{
                HStack{
                    Text("Score \(playerOneName)")
                        .frame(maxWidth: .infinity)
                    Text("Score \(playerTwoName)")
                        .frame(maxWidth: .infinity)
                }
                HStack{
                    Text("\(scorePlayerOne)")
                        .frame(maxWidth: .infinity)
                        .font(.title)
                    Text("\(scorePlayerTwo)")
                        .frame(maxWidth: .infinity)
                        .font(.title)
                }
            }

            
            Spacer()
        }
        .navigationTitle("Mind Memory")
        .onAppear(){
            if !isNetwork{
                cards.generateCard(numberOfPair: 6)
                iniateGame()
            }else{
                iniateGame()
                if sharedListener == nil && isPlayerHost{
                    sharedConnection = nil
                    sharedListener = PeerListener(name: UIDevice.current.name, delegate: self)
                }
                
                if let result = result, !isPlayerHost{
                    sharedConnection = PeerConnection(endpoint: result.endpoint,
                                                      interface: result.interfaces.first,
                                                      delegate: self)
                }
                if let parent = parent {
                    parent.isBack = true
                    //sharedBrowser = nil
                }
            }
            
        }
        .onDisappear(){
            handleBacktoPreviousPage()
        }
    }
    
    func encodeMessageSent(type:String,listCard:[Card]?, index: Int){
        do{
            let message = GameMessage(moveType: type, cards: listCard, index: index)
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(message)
            if let connection = sharedConnection {
                connection.sendMessage(jsonData)
            }
        }catch{
            print(error)
        }
    }
    
    func decodeMessageRecieve(data:Data)->GameMessage?{
        do{
            let jsonDecoder = JSONDecoder()
            let message = try jsonDecoder.decode(GameMessage.self, from: data)
            return message
        }catch{
            print(error)
        }
        return nil
    }
    
    func handleBacktoPreviousPage(){
        do {
            print("Back Button")
            sharedBrowser?.delegate = nil
            sharedConnection?.delegate = nil
            sharedListener?.delegate = nil
            sharedConnection?.cancel()
            sharedConnection = nil
            sharedBrowser?.browser?.cancel()
            sharedBrowser = nil
            sharedListener?.listener?.cancel()
            sharedListener = nil
            if let parent = self.parent {
                parent.isBack = true
                parent.presentationMode.wrappedValue.dismiss()
            } else {
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
    func setHostingRoom() {
        if isPlayerHost {
            sharedListener = PeerListener(name: UIDevice.current.name, delegate: self)
        }
    }
    func setName() {
        if isPlayerHost {
            playerOneName = "Your"
            playerTwoName = "Enemy"
        } else {
            playerOneName = "Enemy"
            playerTwoName = "Your"
        }
    }
    func checkCardClicked(index: Int) {
        if previousIndex == -1 && isAnimateWrong == false && cards.listCard[index].isCannotClose == false {
            flippedCard(index: index)
            previousIndex = index
        } else if isAnimateWrong == false && cards.listCard[index].isCannotClose == false {
            if index != previousIndex {
                isAnimateWrong = true
                flippedCard(index: index)
                if cards.listCard[index].imageName == cards.listCard[previousIndex].imageName {
                    cards.listCard[index].isCannotClose = true
                    cards.listCard[previousIndex].isCannotClose = true
                    calculateScore()
                    nextTurn()
                    chekFinish += 1
                    isfinishGame()
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {[self] in
                        self.flippedCard(index: index)
                        self.flippedCard(index: previousIndex)
                        nextTurn()
                    }
                }
            } else {
                flippedCard(index: index)
                previousIndex = -1
            }
        }
    }
    func calculateScore() {
        if isNetwork {
            calculateScoreNetwork()
        } else {
            calculateScoreLocal()
        }
    }
    func calculateScoreLocal() {
        if isYourTurn {
            scorePlayerOne += 1
        } else {
            scorePlayerTwo += 1
        }
    }
    func calculateScoreNetwork() {
        if isPlayerHost && isYourTurn {
            scorePlayerOne += 1
        } else if isPlayerHost && !isYourTurn {
            scorePlayerTwo += 1
        } else if !isPlayerHost && isYourTurn {
            scorePlayerTwo += 1
        } else if !isPlayerHost && !isYourTurn {
            scorePlayerOne += 1
        }
    }
    func nextTurn() {
        previousIndex = -1
        isAnimateWrong = false
        isYourTurn.toggle()
    }
    func iniateGame() {
        cards.listCard.removeAll()
        if !isNetwork {
            cards.generateCard(numberOfPair: 6)
        }
        isAnimateWrong = false
        previousIndex = -1
        chekFinish = 0
        showingAlert = false
        scorePlayerOne = 0
        scorePlayerTwo = 0
    }
    func resetGame() {
        withAnimation(Animation.easeIn(duration: 1)) {
            cards.generateCard(numberOfPair: 6)
            iniateGame()
        }
    }
    func isfinishGame() {
        if chekFinish == 6 {
            checkWinner()
            chekFinish = 0
        }
    }
    func checkWinner() {
        if isNetwork {
            checkWinnerNetwork()
        } else {
            checkWinnerLocal()
        }
    }
    func checkWinnerLocal() {
        if scorePlayerOne > scorePlayerTwo {
            winnerMessage = "Player One Win"
        } else if scorePlayerOne < scorePlayerTwo {
            winnerMessage = "Player Two Win"
        } else {
            winnerMessage = "Draw"
        }
        showingAlert = true
    }
    func checkWinnerNetwork() {
        if scorePlayerOne > scorePlayerTwo {
            winnerMessage = isPlayerHost ? "You Win" : "You Lost"
        } else if scorePlayerOne < scorePlayerTwo {
            winnerMessage = isPlayerHost ? "You Lost" : "You Wind"
        } else {
            winnerMessage = "Draw"
        }
        showingAlert = true
    }
    func flippedCard(index: Int) {
        withAnimation(Animation.easeIn(duration: 0.5)) {
            cards.listCard[index].isFlipped.toggle()
            cards.objectWillChange.send()
        }
    }
    func getWidth() -> CGFloat {
        let width = UIScreen.main.bounds.width - (30  + 30)
        return width / 3
    }
    func alertGameFinish() -> Alert {
        if isNetwork {
            return alertGameFinishNetwork()
        } else {
            return alertGameFinishLocal()
        }
    }
    func alertGameFinishNetwork() -> Alert {
        Alert(title: Text(winnerMessage), message: Text("Back to main menu?"), dismissButton: .default(Text("Ok")) {
            handleBacktoPreviousPage()
        })
    }
    func alertGameFinishLocal() -> Alert {
        Alert(title: Text(winnerMessage), message: Text("Do you want play again?"), primaryButton: .default(Text("Yes")) {
            self.resetGame()
            showingAlert = false
        }, secondaryButton: .default(Text("No")) {
            self.presentationMode.wrappedValue.dismiss()
        })
    }
}

extension BoardGame: PeerConnectionDelegate {
    func getMessage(content: Data?) {
        guard let content = content else {
            return
        }
        if let message = decodeMessageRecieve(data: content) {
            if message.moveType == "init" && !isPlayerHost {
                setName()
                isStart = true
                if let cards = message.cards {
                    self.cards.listCard = cards
                    self.cards.objectWillChange.send()
                }
            } else if message.moveType == "move"{
                checkCardClicked(index: message.index)
            }
        }
    }
    func connectionReady() {
        isWaitingForPlayer = false
    }
    func connectionFailed() {}
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {}
    func displayAdvertiseError(_ error: NWError) {
        var message = "Error \(error)"
        if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_NoAuth)) {
            message = "Not allowed to access the network"
        }
        print(message)
    }
}
struct BoardGamePreviews: PreviewProvider {
    static var previews: some View {
        BoardGame()
    }
}
