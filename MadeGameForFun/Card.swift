//
//  Card.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import Foundation
import SwiftUI

class Card:ObservableObject,Identifiable, Codable {
    @Published var imageName = ""
    @Published var isFlipped = false
    @Published var isCannotClose = false
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageName = try container.decode(String.self, forKey: .imageName)
        isFlipped = try container.decode(Bool.self, forKey: .isFlipped)
        isCannotClose = try container.decode(Bool.self, forKey: .isCannotClose)
    }
    
    init() {}
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(imageName, forKey: .imageName)
        try container.encode(isFlipped, forKey: .isFlipped)
        try container.encode(isCannotClose, forKey: .isCannotClose)
    }
    
    enum CodingKeys: CodingKey {
        case imageName
        case isFlipped
        case isCannotClose
    }
}
