//
//  CardManager.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import Foundation
import SwiftUI
class CardManager:ObservableObject {
    @Published var listCard = [Card]()
    var listOfEmoji = ["😀","❤️","🐍","🏵","☀️"]
    init(numberOfPair: Int) {
        //generateCard(numberOfPair: numberOfPair)
    }
    func generateCard(numberOfPair: Int) {
        listCard.removeAll()
        for _ in 1...numberOfPair {
            let randomNumber = Int.random(in: 0..<listOfEmoji.count)
            let cardOne = Card()
            cardOne.imageName = listOfEmoji[randomNumber]
            listCard.append(cardOne)
            let cardTwo = Card()
            cardTwo.imageName = listOfEmoji[randomNumber]
            listCard.append(cardTwo)
        }
        listCard.shuffle()
    }
}
