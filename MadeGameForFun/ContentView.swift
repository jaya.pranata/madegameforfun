//
//  ContentView.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import Foundation
import MultipeerConnectivity
import SwiftUI
import Network
import UIKit

struct ContentView: View {
    var body: some View {
        NavigationView{
            VStack{
                
                NavigationLink(destination: BoardGame(isStart:true)) {
                    ButtonView(text: "Local Game")
                        .frame(width: 200 ,  alignment: .center)
                }
                
                NavigationLink(destination: BoardGame(isPlayerHost:true, isStart: false, isYourTurn: true,isNetwork: true)) {
                    ButtonView(text: "Host Game")
                        .frame(width: 200 ,  alignment: .center)
                }.padding(.top, 8)
                
                NavigationLink(destination: FindRoom()) {
                    ButtonView(text: "Find Room")
                        .frame(width: 200 ,  alignment: .center)
                }
                .padding(.top, 8)
                
                
                .navigationBarTitle("Game Option")
                Spacer()
            }
            .padding(.top, 40)
            //.navigationTitle("Main")
            .preferredColorScheme(/*@START_MENU_TOKEN@*/.dark/*@END_MENU_TOKEN@*/)
        }
    }
}

struct ButtonView: View {
    @State var text:String = ""
    var body: some View {
        Text(text)
            .frame(width: UIScreen.main.bounds.width - 60 ,  alignment: .center)
            //.frame(width: 200, height: 100, alignment: .center)
            .padding(.vertical, 20)
            .background(Color.blue)
            .foregroundColor(Color.white)
            .cornerRadius(50)
    }
}

extension ContentView: PeerBrowserDelegate{
    func refreshResults(results: Set<NWBrowser.Result>){
        
    }
    func displayBrowseError(_ error: NWError){
        
    }
}

struct ContentViewPreviews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct Home:View {
    @State var moves:[String] = Array(repeating: "", count: 10)
    @State var isPlaying = false
    var body: some View {
        VStack{
            LazyVGrid(columns: Array(repeating: GridItem(.flexible(),spacing: 15), count: 3)){
                ForEach(0..<10,id: \.self){index in
                    ZStack{
                        Color.blue
                        
                        Color.white
                            .opacity(moves[index] == "" ? 1 : 0)
                        Text(moves[index])
                            .font(.system(size: 55))
                            .fontWeight(.black)
                            .foregroundColor(.white)
                            .opacity(moves[index] != "" ? 1 : 0)
                    }
                    .frame(width: getWidth(), height: getWidth())
                    .cornerRadius(15)
                    .rotation3DEffect(
                        .init(degrees: moves[index] != "" ? 180 : 0 ) ,
                        axis: (x: 0.0, y: 1.0, z: 0.0),
                        anchor: .center,
                        anchorZ: 0.0,
                        perspective: 1.0
                    )
                    .onTapGesture(perform: {
                        withAnimation(Animation.easeIn(duration: 0.5)){
                            if moves[index] == ""{
                                moves[index] = isPlaying ? "X" : "O"
                                isPlaying.toggle()
                            }else{
                                moves[index] = ""
                                isPlaying.toggle()
                            }
                        }
                    })
                }
            }
            .padding(15)
        }
    }
    
    func getWidth() -> CGFloat{
        let width = UIScreen.main.bounds.width - (30  + 30)
        return width / 3
    }
}
