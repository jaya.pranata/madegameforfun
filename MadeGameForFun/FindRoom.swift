//
//  FindRoom.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import SwiftUI
import Network

struct FindRoom: View {
    @State var results: [NWBrowser.Result] = [NWBrowser.Result]()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var isBack = false
    var body: some View {
        List {
            if results.count > 0 {
                ForEach(results, id: \.self) { result in
                    NavigationLink(destination: BoardGame(isPlayerHost:false, isStart:false, isYourTurn: false, isNetwork: true, result: result, parent: self)) {
                        let peerEndpoint = result.endpoint
                        if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = peerEndpoint {
                            Text(name)
                        }                        
                    }
                }
            }
        }
        .onAppear {
            if isBack {
                results.removeAll()
                self.presentationMode.wrappedValue.dismiss()
            } else if sharedBrowser == nil {
                sharedBrowser = PeerBrowser(delegate: self)
            } else {
                if let browser = sharedBrowser?.browser {
                    for result in browser.browseResults {
                        if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = result.endpoint {
                            if name != UIDevice.current.name {
                                self.results.append(result)
                            }
                        }
                    }
                }
            }
        }
        .onDisappear {
            print("Disappear")
        }
    }
}

extension FindRoom: PeerBrowserDelegate {
    func refreshResults(results: Set<NWBrowser.Result>) {
        self.results = [NWBrowser.Result]()
        for result in results {
            if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = result.endpoint {
                if name != UIDevice.current.name {
                    self.results.append(result)
                }
            }
        }
    }
    func displayBrowseError(_ error: NWError) {
        var message = "Error \(error)"
        if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_NoAuth)) {
            message = "Not allowed to access the network"
        } else {
            print(message)
        }
    }
}

struct FindRoomPreviews: PreviewProvider {
    static var previews: some View {
        FindRoom()
    }
}
