//
//  NWPeerParameter.swift
//  MadeGameForFun
//
//  Created by Jaya Pranata on 10/13/20.
//

import Network
import CryptoKit

extension NWParameters {

    // Create parameters for use in PeerConnection and PeerListener.
    convenience init() {
        // Customize TCP options to enable keepalives.
        let tcpOptions = NWProtocolTCP.Options()
        tcpOptions.enableKeepalive = true
        tcpOptions.keepaliveIdle = 2

        self.init(tls: .none, tcp: tcpOptions)
        
        // Enable using a peer-to-peer link.
        self.includePeerToPeer = true

        // Add your custom game protocol to support game messages. 
        let gameOptions = NWProtocolFramer.Options(definition: GameProtocol.definition)
        self.defaultProtocolStack.applicationProtocols.insert(gameOptions, at: 0)
    }
}
